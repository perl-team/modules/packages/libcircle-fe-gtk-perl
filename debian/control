Source: libcircle-fe-gtk-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Andrej Shadura <andrewsh@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 12),
               libmodule-build-perl,
               perl
Build-Depends-Indep: libfile-sharedir-perl <!nocheck>,
                     libglib-perl <!nocheck>,
                     libgtk2-perl <!nocheck>,
                     libio-async-loop-glib-perl <!nocheck>,
                     libio-async-perl <!nocheck>,
                     libmodule-pluggable-perl <!nocheck>,
                     libnet-async-tangence-perl <!nocheck>,
                     libtangence-perl <!nocheck>,
                     libvariable-disposition-perl <!nocheck>
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcircle-fe-gtk-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcircle-fe-gtk-perl.git
Homepage: https://metacpan.org/release/circle-fe-gtk

Package: circle-gtk
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libfile-sharedir-perl,
         libglib-perl,
         libgtk2-perl,
         libio-async-loop-glib-perl,
         libio-async-perl,
         libmodule-pluggable-perl,
         libnet-async-tangence-perl,
         libtangence-perl,
         libvariable-disposition-perl
Suggests: circle-backend
Provides: libcircle-fe-gtk-perl
Description: GTK 2 frontend for the Circle IRC client
 Circle is an IRC client which merges the best attributes of a local
 client and the common screen+irssi recipe:
 .
  * Keep IRC connects and state on a backend server, allowing
    disconnections from local UI.
  * Interact with a real local GUI for the frontend, instead of
    incurring SSH roundtrips for every keypress.
 .
 Circle uses Tangence for its underlying communications layer.
 .
 This package provides a GTK 2 user interface for Circle. It needs
 circle-backend to be installed on the same or a different machine
 in order to be useful.
